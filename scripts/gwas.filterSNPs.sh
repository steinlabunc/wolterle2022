#!/bin/bash
source ~/.bashrc

# Wnt Phase 1 GWAS pipeline: 01_FilterSNPs.sh
# 01 Filter variants by genotypic frequency
# Brandon Le 210216 / The Stein Lab / UNC

### SCRIPT OVERVIEW
## three jobs submitted:
#	1) prepare .tped/.tfam genotype files for genetic association with EMMAX
#	2) prepare masked .tped/.tfam genotype files for constructing kinship matrices
#	3) create Balding-Nichols kinship matrix for controlling local ancestry

module load plink
module load emmax

### Job 1:  Create genotype frequency filtered .tped/.tfam files (PLINK)
## Variables passed to this command from 01_FilterSNPs.R
# $1 path to genotype files (TOPMed imputed)
# $2 path to list of genotype-frequency filtered SNPs
# $3 sample keep list (hNPC donors included in GWAS)
# $4 path to output

echo ">>> Applying GenoFreq Filtering on .bed/.bim/.fam " $1
echo ">>> Making .tped/.tfam genotype files " $4

jid1=$(ssub --mem=8g --notify=OFF --wrap=\"plink \
	--bfile $1 \
	--extract $2 \
	--keep $3 \
	--output-missing-genotype 0 \
	--recode 12 transpose \
	--out $4\")

### Job2: Create masked .tped/.tfam files for making kinship matrices (PLINK)
## Variables passed to this command from 01_FilterSNPs.R
# $5 path to raw directly genotyped files
# $6 chromosome to mask
# $7 path to output

echo ">>> Making .tped/.tfam genotype files MASKED from chr: " $6
jid2=$(ssub --mem=8g --notify=OFF --wrap=\"plink \
	--bfile $5 --keep $3 --output-missing-genotype 0 \
	--recode 12 transpose \
	--not-chr $6 \
	--out $7\")

### Job3: Create Balding-Nichols kinship matrix for controlling local ancestry (EMMAX)
## SUBMISSION DEPENDENT ON JOB2 COMPLETION
## Variables passed to this command
# $7 kinship matrix created by Job2

echo ">>> Making Balding Nichols Kinship Matrix for " $7
jid3=$(ssub -d afterok:$jid2 --mem=16g --notify=OFF --wrap=\"emmax-kin \
	-v -d 10 $7\")
