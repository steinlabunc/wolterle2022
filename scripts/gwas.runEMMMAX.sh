#!/bin/bash
source ~/.bashrc

# EMMAX GWAS Submission Script
# Brandon Le 210217
# Receives inputs passed from 02_runGWASemmax.R

# $1 FRQX-filtered genotype file *.tped
# $2 Phenotype file  
# $3 Kinship matrix *.BN.kinf
# $4 covarate table
# $5 output path

module load emmax

jid=$(ssub -p steinlab --mem=16g --notify=OFF --wrap=\"emmax -v -d 10 -t $1 -p $2 -k $3 -c $4 -o $5\")
