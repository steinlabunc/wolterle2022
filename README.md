Repository for Wolter, Le, et al 2022  
[Stein Lab](https://www.steinlab.org)  
[https://bitbucket.org/steinlabunc/wolterle2022](https://bitbucket.org/steinlabunc/wolterle2022)  

| directories | description |   
| ----------- | ----------- |
| gwasSummaryStatistics	| variant level summary statistics separated by treatment condition |    
| proliferationAssayData | raw data from EdU-incorporation proliferation assay |  
| scripts | project scripts see below |  


| script | description |
| ------ | ----------- |
| flowDensityGating.prolifAssay.R | process proliferation assay flow cytometry data |  
| gwas.filterSNPs.sh | prepare genotype data for genetic association |  
| gwas.runEMMMAX.sh | run genetic association with EMMAX software |  
| pAdj.R | calculate a study-wide significance value |  
| qqEnrichment.prolifXtrait.R | filter GWAS summary statistic variants by p-value in lithium-sensitive hNPC proliferation GWAS and visualize enrichment with quantile quantile (qq) plots |  

-Brandon Le 220130  
